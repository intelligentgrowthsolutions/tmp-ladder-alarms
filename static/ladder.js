const ncols = 5;
const nrows = 10;

function mkDD(ls,onchange){
  return $m('select',{onchange:onchange,disabled:false},ls.map(i=>$m('option',{},i)));
}

function reportSystemState(){
  let ret = Array.from(document.querySelectorAll('tr.one-alarm'))
    .map(tr=>Array.from(tr.querySelectorAll('select')).map(s=>s.value));
  console.log(ret);
}

let l1dd = ['time-condition', 'alarmIfLow','alarmIfHigh', 'alarmIfDelta', 'site-condition',];
$i('content').appendChild($m('table',{},
  Array.from(Array(nrows).keys()).map(j =>
    $m('tr',{'class':'one-alarm'}, Array.from(Array(ncols).keys()).map(i=> $m('td',{}, [
      mkDD(['',...l1dd], ()=>{
        let pgh = event.target.parentNode.querySelector('.params-go-here');
        pgh.innerHTML = '';
        pgh.appendChild($m('div',{},[
          $m('label',{},'Threshold'),
          $m('input',{style:'width:3ex'},'')
        ]));
        return;
      }),
      $m('div',{'class':'params-go-here'},'xx')
    ])))
  )
));

$i('content').appendChild($m('button',{'class':'btn',onclick:()=>{
  reportSystemState();
}},'Report System Status'));
