
let $i = id => document.getElementById(id);
function $m(tag,props,children){
  let args = [...arguments];
  let ret = document.createElement(args.shift());
  for(let a of args){
    if (typeof a === "string")
      ret.innerHTML = a;
    else if (Array.isArray(a)){
      for(let c of a){
        ret.appendChild(c);
      }
    }else{
      for(let k in a){
        if (k === 'text'){
          ret.innerText = a[k];
        }else if (k.startsWith('on'))
          ret.addEventListener(k.substr(2),a[k]);
        else
          ret.setAttribute(k,props[k]);
      }
    }
  }
  return ret;
}

